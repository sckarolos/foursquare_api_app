(function () {
    angular.module('CodeTaskApp', []);

    angular.module('CodeTaskApp').controller('MainController', function ($scope, venuesSearchService) {

        var mainScope = this;
        mainScope.items = [];
        var data = [];
        var latCenter = [];
        var lngCenter = [];
        var markers =[];

        mainScope.category = '';
        // initialise place to london
        mainScope.place = 'London';

        this.doExplore = function () {
    			mainScope.selectedIndex = -1;

          markers.splice(0,markers.length);
          data.splice(0,data.length);

          // Call the async method to hit the api
          venuesSearchService.async(this.category, this.place).then(function (items) {
              mainScope.items = items;

              items.forEach(function(item){
                data.push(item.venue);
              });
              // redraw map
              initMap();
          });
        };

        this.doExplore();

        this.getCategory = function (venue) {
          var category = '';
          if (venue.categories.length > 0)
              category = venue.categories[0].name;
          return category;
        };
        // init google map
        function initMap() {
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(data[0].location.lat, data[0].location.lng),
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            // style the google map
            styles: [
                    {
                        "featureType": "all",
                        "elementType": "all",
                        "stylers": [
                            {
                                "hue": "#00ffbc"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": -70
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "saturation": -60
                            }
                        ]
                    }
                ]
          });

          var infowindow = new google.maps.InfoWindow();

          for (var i in data) {
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(data[i].location.lat), parseFloat(data[i].location.lng)),
                map: map,
                animation: google.maps.Animation.DROP // drop new icons after refresh.
              });
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  // marker popup on click
                  infowindow.setContent('<div style="font-weight: 600; color: #393939;">' + data[i].name + '</div></br>' +
                                        '<div><span>Address : </span>' + data[i].location.address + '</div>');
                  infowindow.open(map, marker);
                    //change center to marker's position
                    map.setCenter(marker.getPosition());
                }
              })(marker, i));
          }
        }
    });
})();
