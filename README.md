### Demo URL: http://ktalvis.com/source

### App description: 
- Single page app that integrates with foursquare and google maps api.
- A user can type a location so the app returns the 10 most recommended venues for this location.
- The results are being displayed on a table and visualised on a google map.
- Every map marker contains a popup with basic information for this venue.

### How to install on localhost:
- Clone the repository.
- This application runs on Node.js. Install dependencies by typing:

    $ npm install

## How to run on localhost:
- Run locally by typing:  

    $ node app.js

The app is set to be listening to port: 8888 (http://localhost:8888)

## Tecnhologies used:
- Bootstrap 3.3.7
- AngularJS 1.4.7
- Native JS
- CSS
- HTML
- Google maps API
- Foursquare API


### Comments:  
- Some frameworks/libraries like bootstrap or Jquery are being called via cdn urls in order to reduce the size of the application. AngularJS is installed via npm and node.js in order to show
- how a dependency can be handled. 


## Karolos Talvis - 28/3/18  


